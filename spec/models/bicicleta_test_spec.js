var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta.model");

describe("Testing Bicicletas", function () {
  // beforeEach(function (done) {
  //   var mongoDB = "mongodb://localhost/testdb";
  //   mongoose.connect(mongoDB, { useNewUrlParser: true });

  //   var db = mongoose.connection;
  //   db.on("error", console.error.bind(console, "Connection error"));
  //   db.once("open", function () {
  //     console.log("We are connected to test database");
  //     done();
  //   });
  // });

  beforeAll(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";

      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });

      mongoose.set("useCreateIndex", true);

      var db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB connection error: "));
      db.once("open", function () {
        console.log("We are connected to test database!");
        done();
      });
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.createInstance", () => {
    it("Crea una instancia d bicicleta", () => {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toEqual(-34.5);
      expect(bici.ubicacion[1]).toEqual(-54.1);
    });
  });

  describe("Bicicleta.allBici", () => {
    it("Comienza vacío", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe("Bicicleta.add", () => {
    it("Agregar sólo una bicicleta", (done) => {
      var a = new Bicicleta({ code: 1, color: "rojo", modelo: "urbana" });
      Bicicleta.add(a, function (err, newBici) {
        if (err) console.log(err);

        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toBe(1);
          expect(bicis[0].code).toEqual(a.code);
          done();
        });
      });
    });
  });

  describe("Bicicleta.findByCode", () => {
    it("Debe devolver la bicicleta con código 1", (done) => {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);

        var a = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
        Bicicleta.add(a, function (err, newBici1) {
          if (err) console.log(err);

          var b = new Bicicleta({ code: 1, color: "rojo", modelo: "urbana" });
          Bicicleta.add(b, function (err, newBici2) {
            if (err) console.log(err);

            Bicicleta.findByCode(1, function (err, targetBici) {
              expect(targetBici._id).toBe(1);
              expect(targetBici.color).toBe(a.color);
              expect(targetBici.modelo).toBe(a.modelo);
            });
          });
        });

        done();
      });
    });
  });

  //
});
