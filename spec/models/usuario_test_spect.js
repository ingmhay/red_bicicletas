var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta.model");
var Usuario = require("../../models/usuario.model");
var Reserva = require("../../models/reserva.model");
//const { db } = require("../../models/bicicleta.model");

describe("Testing Usuario", function () {
  beforeEach(function (done) {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });
    db = mongoose.connection;
    db.on("error", console.error.bind(console, "Connetion error"));
    db.once("open", function () {
      console.log("We are connected to test database");
      done();
    });
  });

  afterEach(function (done) {
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function (err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function (err, success) {
          if (err) console.log(err);
          done();
        });
      });
    });
  });

  describe("Cuando un usuario reserva una bicicleta", () => {
    it("desde, reserva", (done) => {
      const usuario = new Usuario({ nombre: "Arley" });
      usuario.save();

      var bicicleta = new Bicicleta({
        code: 1,
        color: "rojo",
        modelo: "urbana",
      });

      var hoy = new Date();
      var tomorrow = new Date();
      tomorrow.setDate(hoy.getDate() + 1);
      usuario.reservar(bicicleta.id, hoy, tomorrow, function (err, reserva) {
        Reserva.find({})
          .populate("bicicleta")
          .populate("usuario")
          .exec(function (err, reserva) {
            console.log("reserva[0]");
            console.log(reserva[0]);
            expect(reserva.length).toBe(1);
            expect(reserva[0].diasDeReserva()).toBe(2);
            // expect(reserva[0].bicicleta.code()).toBe(1);
            expect(reserva[0].usuario.nombre).toBe(usuario.nombre);
            done();
          });
      });
    });
  });
});
