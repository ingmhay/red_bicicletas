var mongoose = require("mongoose");
var Bicicleta = require("../../models/bicicleta.model");
var server = require("../../bin/www");
var request = require("request");
const { json } = require("express");

var base_url = "http://localhost:5000/api/bicicletas";

describe("Bicicleta API", () => {
  // beforeAll((done) => {
  //   mongoose.connection.close(done);
  // });

  // beforeEach(function (done) {
  //   var mongoDB = "mongodb://localhost/testdb";
  //   mongoose.connect(mongoDB, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true,
  //   });

  //   var db = mongoose.connection;
  //   db.on("error", console.error.bind(console, "MongoDB connection error: "));
  //   db.once("open", function () {
  //     console.log("We are connected to test database!");
  //     done();
  //   });
  // });

  beforeAll(function (done) {
    mongoose.connection.close().then(() => {
      var mongoDB = "mongodb://localhost/testdb";

      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });

      mongoose.set("useCreateIndex", true);

      var db = mongoose.connection;
      db.on("error", console.error.bind(console, "MongoDB connection error: "));
      db.once("open", function () {
        console.log("We are connected to test database!");
        done();
      });
    });
  });

  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Get Bicicletas /", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        console.log(result);
        expect(response.statusCode).toBe(200);
        // expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("Post Bicicletas /create", () => {
    it("Status 200", (done) => {
      var header = { "content-type": "application/json" };
      var aBici =
        '{ "code": "10", "color": "rojo", "modelo": "urbana", "ubicacion":[3.3950619,  -76.5957048] }';

      request.post(
        {
          headers: header,
          url: base_url + "/create",
          body: aBici,
        },
        function (error, response, body) {
          // if (err) console.log(err);
          expect(response.statusCode).toBe(200);
          var bici = JSON.parse(body);
          console.log(bici);
          expect(bici.color).toBe("rojo");
          done();
        }
      );
    });
  });
});
