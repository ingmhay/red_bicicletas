Proyecto Red-Bicicletas que es la aplicación práctica del curso “Desarrollo del lado servidor: NodeJS, Express y MongoDB”, donde se aplican los diferentes conceptos abordados por el tutor.

Se puede acceder a los fuentes del proyecto en la siguiente URL: https://ingmhay@bitbucket.org/ingmhay/red_bicicletas.git, los cuales pueden ser descargados usando el siguiente comando:

git clone https://ingmhay@bitbucket.org/ingmhay/red_bicicletas.git

Quiero agradecer al excelente instructor del curso Ezequiel Lamónica, por sus valiosos conocimientos impartidos de una forma clara y práctica. También agradezco por su tiempo y esfuerzo a los compañeros estudiantes que revisaran mis tareas, de antemano muchas gracias.
