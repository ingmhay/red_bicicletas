require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("./config/passport");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const jwt = require("jsonwebtoken");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var tokenRouter = require("./routes/token.router");
var bicicletasRuter = require("./routes/bicicletas.router");
var bicicletasAPIRuter = require("./routes/api/bicicletaAPI.router");
var usuariosAPIRuter = require("./routes/api/usuarioAPI.router");
var authAPIRuter = require("./routes/api/auth");
const Usuario = require("./models/usuario.model");
const Token = require("./models/token.model");

let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "session",
  });

  store.on("error", function (err) {
    assert.ifError(err);
    assert.ok(false);
  });
}

var app = express();
app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 100 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: "red_bici_arley",
  })
);

var monogoose = require("mongoose");
const { assert } = require("console");
/* 
  El paquete node "dotenv", no servira para gestión de variables de ambientes
  Util aqui para no quemar la url de la base de datos, para lograrlo que requiere:
  1) Crear un archivo ".env" con las variables en el formato KEY='valor',
     en este estarán los datos del ambiente de desarrollo
  2) adicionar el archivo '.env' a gitignore parar que no se versione ni se incluya en 
     los deploye
  3) Anivel del servidor de heroku se deben crear las variables de ambeintes (con el mismo
      key) y los valores de producción.
 */

//mongodb://localhost/red_bicicletas
//mongodb+srv://admin:<password>@cluster-red-bici-mhay.acach.mongodb.net/<dbname>?retryWrites=true&w=majority

var mongoDB = process.env.MONGO_URI;

monogoose.connect(mongoDB, { useNewUrlParser: true });
monogoose.Promise = global.Promise;
var db = monogoose.connection;

db.on("error", console.error.bind(console, "Mongo conection error."));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get("/login", function (req, res) {
  res.render("session/login");
});

app.post("/login", function (req, res, next) {
  passport.authenticate("local", function (err, usaurio, info) {
    if (err) return next(err);

    if (!usaurio) return res.render("session/login", { info });

    req.logIn(usaurio, function (err) {
      if (err) return next(err);

      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  res.redirect("/");
});

app.get("/forgotPassword", function (req, res, next) {
  res.render("session/forgotPassowrd");
});

app.post("/forgotPassword", function (req, res) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usaurio)
      return res.render("session/forgotPassword", {
        info: { message: "No exsite el email para un usuario existente" },
      });

    Usuario.resetPassowrd(function (err) {
      if (err) return next(err);
      console.log("session/fotgotPasswordMessage");
    });

    res.render("session/fotgotPasswordMessage");
  });
});

app.get("/resetPassWord/:token", function () {
  Token.findOne({ token: req.body.token }, function (err, token) {
    if (!token)
      return res.status(400).send({
        type: "no-verified",
        msg:
          "No existe password asociado al token. Verifique que su token no haya expirado",
      });

    Usuario.findById(token._userId, function (err, usuario) {
      if (!usuario)
        return res
          .status(400)
          .send({ msg: "No existe un usuario asociado al token" });

      res.render("session/resetPassword", { errors: {}, usuario: usuario });
    });
  });
});

app.post("/resetPassWord", function (req, res) {
  if (req.body.password !== req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: { message: "No coincide con el password ingresado" },
      },
      usuario: new Usuario({ email: req.body.email }),
    });
    return;
  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function (err) {
      if (err) {
        res.render("session/resetPassword", {
          errors: err.errors,
          usuario: new Usuario({ email: req.body.email }),
        });
      } else {
        res.redirect("/login");
      }
    });
  });
});

app.use("/", indexRouter);
app.use("/usuarios", usersRouter);
app.use("/token", tokenRouter);
app.use("/bicicletas", loggedIn, bicicletasRuter);

app.use("/api/auth", authAPIRuter);
app.use("/api/bicicletas", validarUsuario, bicicletasAPIRuter);
app.use("/api/usuarios", usuariosAPIRuter);
app.use("/policy_privacy", function (req, res) {
  res.sendFile("public/policy_privacy.html");
});
app.use("/google30eb2304897c602b", function (req, res) {
  res.sendFile("public/google30eb2304897c602b.html");
});

app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/plus.login",
      "https://www.googleapis.com/auth/plus.profile.emails.read",
    ],
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/error",
  })
);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("usuario sin loguearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(
    req.header["x-access-token"],
    req.app.ge("secretKey"),
    function (err, decode) {
      if (err) {
        res.json({ status: "error", message: err.message, data: null });
      } else {
        req.body.userId = decode.id;
        console.log("jwt verify", decode);
        next();
      }
    }
  );
}

module.exports = app;
