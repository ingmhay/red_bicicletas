var Bicicleta = require("../models/bicicleta.model");

exports.Bicicleta_list = function (req, res) {
  Bicicleta.allBicis(function (err, bicis) {
    res.render("./bicicletas/index", { bicis: bicis });
  });
};

exports.Bicicleta_create_get = function (req, res) {
  res.render("./bicicletas/create");
};

exports.Bicicleta_create_post = function (req, res) {
  const { id, color, modelo, lat, lng } = req.body;
  var bici = new Bicicleta({
    code: id,
    color: color,
    modelo: modelo,
    ubicacion: [lat, lng],
  });

  Bicicleta.add(bici);

  res.redirect("/bicicletas");
};

exports.Bicicleta_update_get = function (req, res) {
  const bici = Bicicleta.findById(req.params.id, function (err, bici) {
    if (err) console.log(err);
    console.log(bici);
    res.render("bicicletas/update", { bici });
  });
};

exports.Bicicleta_update_post = function (req, res) {
  const { id, color, modelo, lat, lng } = req.body;

  const filter = { _id: req.params.id };
  const update = { modelo: modelo, color: color };

  var bici = Bicicleta.findOneAndUpdate(filter, update, function (err, bici) {
    res.redirect("/bicicletas");
  });
};

exports.Bicicleta_delete_post = function (req, res) {
  const { id } = req.body;
  console.log("Bicicleta_delete_post.id", id);
  Bicicleta.removeById(id, (err, bici) => {
    res.redirect("/bicicletas");
  });
};
