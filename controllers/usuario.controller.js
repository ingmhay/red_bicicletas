var Usuario = require("../models/usuario.model");

module.exports = {
  usuarios_list: function (req, res, next) {
    Usuario.find({}, (err, usuarios) => {
      res.render("usuarios/index", { usuarios: usuarios });
    });
  },
  update_get: function (req, res, next) {
    Usuario.findById(req.params.id, function (err, usuario) {
      res.render("./usuarios/update", { errors: {}, usuario: usuario });
    });
  },
  update: function (req, res, next) {
    var update_values = { nombre: req.body.nombre };

    Usuario.findByIdAndUpdate(
      req.params.id,
      update_values,
      function (err, usuario) {
        if (err) {
          console.error(err);
          res.render("usuario/update", {
            errors: err.errors,
            usuario: new Usuario(update_values),
          });
        } else {
          res.redirect("/usuarios");
          return;
        }
      }
    );
  },
  create_get: function (req, res, next) {
    console.log("create_get");
    res.render("usuarios/create", { errors: {}, usuario: new Usuario() });
  },
  create: function (req, res, next) {
    const { nombre, email } = req.body;
    console.log(nombre, email);

    if (req.body.password !== req.body.confirm_password) {
      res.render("usuario/create", {
        errors: {
          confirm_password: {
            message: "No coincide con el password ingresado",
          },
        },
        usuario: new Usuario({ nombre: nombre, email: email }),
      });
      return;
    }

    Usuario.create(
      { nombre: nombre, email: email, password: req.body.password },
      function (err, newUsuario) {
        if (err) {
          console.error(err);
          res.render("usuario/create", {
            errors: err.errors,
            usuario: new Usuario(nombre, emai),
          });
        } else {
          newUsuario.enviar_email_bienvenida();
          res.redirect("/usuarios");
        }
      }
    );
  },
  delete: function (req, res, mext) {
    Usuario.findByIdAndDelete(req.body.id, function (err) {
      if (err) {
        next(err);
      } else {
        res.redirect("/usuarios");
      }
    });
  },
};
