var Usuario = require("../models/usuario.model");
var Token = require("../models/token.model");
const { findOne } = require("../models/token.model");

module.exports = {
  confirmationGet: function (req, res, next) {
    Token,
      findOne({ token: req.params.token }, function (err, token) {
        if (!token)
          return res
            .status(400)
            .send({ type: "Not-verified", msg: "No al...." });

        Usuario.findById(token._userId, function (err, usuario) {
          if (!usuario) {
            return res.status(400).send({
              type: "User not found",
              msg:
                "No encontramos un usuario con este token o el token ya expiró",
            });
          }

          if (usuario.verificado) return res.redirect("/usuarios");
          usuario.verificado = true;

          usuario.save(function (err) {
            if (err) {
              return res.status(500).send({ msg: err.message });
            }
            res.redirect("/");
          });
        });
      });
  },
};
