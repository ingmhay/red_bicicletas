var Bicicleta = require("../../models/bicicleta.model");

exports.Bicicleta_list = function (req, res) {
  res.status(200).json({ bicicletas: Bicicleta.allBicis });
};

exports.Bicicleta_create = function (req, res) {
  const { id, color, modelo, lat, lng } = req.body;

  var bici = new Bicicleta(id, color, modelo, [lat, lng]);
  Bicicleta.add(bici);

  res.status(200).json({ bicicleta: bici });
};

exports.Bicicleta_update = function (req, res) {
  const { id, color, modelo, lat, lng } = req.body;
  var bici = Bicicleta.findById(req.params.id);

  bici.modelo = modelo;
  bici.color = color;
};

exports.Bicicleta_delete = function (req, res) {
  const { id } = req.body;
  Bicicleta.removeById(id);

  res.status(204).send();
};
