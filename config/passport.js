const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const Usuario = require("../models/usuario.model");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookTokenStrategy = require("passport-facebook-token");

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_APP_ID,
      clientSecret: process.env.FACEBOOK_APP_SECRET,
      fbGraphVersion: "v3.0",
    },
    function (accessToken, refreshToken, profile, done) {
      try {
        Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
          if (err) console.log("err", err);
          return done(err, user);
        });
      } catch (error) {
        console.log(error);
        return done(error, null);
      }
      // User.findOrCreate({ facebookId: profile.id }, function (error, user) {
      //   return done(error, user);
      // });
    }
  )
);

passport.use(
  new localStrategy(function (email, password, done) {
    Usuario.findOne({ email: email }, function (err, usuario) {
      if (err) return done(err);
      if (!usuario)
        return done(null, false, { message: "Email no existe o incorrecto" });

      console.log("password", password);
      if (usuario.validPassword(password)) return "Contraseña incorrecta";

      return done(null, usuario);
    });
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.HOST + "/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, cb) {
      console.log(profile);
      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
      });
    }
  )
);

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});
passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});
module.exports = passport;
