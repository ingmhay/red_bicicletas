const express = require("express");
const passport = require("passport");
const router = express.Router();
const authController = require("../../controllers/api/authControllerAPI");

router.post("/authenticate");
router.post("/forgotPassword");
router.post(
  "/facebook_token",
  passport.authenticate("facebook-token"),
  authController.authFacebookToken
);

module.exports = router;
