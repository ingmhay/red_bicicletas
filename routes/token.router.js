var express = require("express");
var router = express.Router();
var tokenController = require("../controllers/token.controller");
const { model } = require("../models/token.model");

router.get("/confirmation/token", tokenController.confirmationGet);

module.exports = router;
