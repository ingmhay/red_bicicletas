var express = require("express");
var router = express.Router();
var usuarioController = require("../controllers/usuario.controller");

router.get("/", usuarioController.usuarios_list);
router.get("/create", usuarioController.create_get);
router.post("/create", usuarioController.create);
// router.post("/reservar", usuarioController.);
module.exports = router;
