//main_map es del div contenedor del mapa
var map = L.map("main_map").setView([3.369053, -76.510347], 13);

L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

// L.marker([3.3950619, -76.5957048]).addTo(map).bindPopup("Test.").openPopup();
// L.marker([3.3629818, -76.5555981]).addTo(map);
// L.marker([3.358676, -76.5420048]).addTo(map);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function (result) {
    console.log(result);
    result.bicicletas.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici._id }).addTo(map);
    });
  },
});
