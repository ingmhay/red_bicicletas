var mongoose = require("mongoose");
var Reserva = require("./reserva.model");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const mongooseUniqueValidator = require("mongoose-unique-validator");
//const { getMaxListeners } = require("./reserva.model");

const Token = require("./token.model");
const mailer = require("../mailer/mailer");
const { setMaxListeners } = require("../app");
const { strict } = require("assert");

const saltRounds = 10;
var Schema = mongoose.Schema;

const validateMail = function (email) {
  const re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    unique: true,
    required: [true, "El correo es obligatoria"],
    lowercase: true,
    validate: [validateMail, "Por favor ingrese un correo válido"],
    match: [/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/],
  },
  password: {
    type: String,
    required: [true, "La contraseña es obligatoria"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: { type: Boolean, default: false },
  googleId: String,
  facebookId: String,
});

/* 
  Como la DB de Mongo por defecto no trabaja con constraints de unicidad
  se Instalamos la librería “mongoose-unique-validator” y se importa como
  un plugin
*/
usuarioSchema.plugin(mongooseUniqueValidator, {
  message: "El {PATH} ya existe con otro usuario",
});

/* Vamos a trabajar con los eventes de MongoDB 
usuarioSchema.pre('save') => antes de guardar
*/
usuarioSchema.pre("save", function (next) {
  //vamos a cifrar el passowd
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  console.log(this.password);
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta: biciId,
    desde: desde,
    hasta: hasta,
  });
  console.log(reserva);
  reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
  const token = new Token({
    _userId: this._id,
    token: crypto.randonByTest(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err);
    }
    const mailOptions = {
      from: "no-reply@bicicletas.com",
      to: email_destination,
      suject: "Verificación de cuenta",
      text:
        "Hola \n\n Por favor, para verificar su cuenta haga clic en este link\n http://localhost:5000/token/confirmation/" +
        token.token +
        "\n",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) {
        return console.log(err);
      }

      console.log("A verification email has been sent to " + email_destination);
    });
  });
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this._id,
    token: crypto.randomBytes(16).toString("hex"),
  });

  const email_destination = this.email;

  token.save(function (err) {
    if (err) return cb(err);

    const mailOptions = {
      from: "no-reply@red-bicicletas.com",
      to: email_destination,
      subject: "Reseteo de password de cuenta",
      text:
        "Hola, \n\n por favor para resetear el password de su cuenta haga clic en este link ....",
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return cb(err);
      console.log("Correo enviado");
    });
    cb(null);
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function (condition, callback) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ googleId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("============= CONDITION =============");
        console.log(condition);
        let values = {};
        values.googleId = condition.googleId;
        values.email = condition.emails[0].value;
        value.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = condition._json.etap;
        console.log("============= VALUES =============");
        console.log(values);

        setMaxListeners.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          callback(err, result);
        });
      }
    }
  );
};
usuarioSchema.statics.findOneOrCreateByFacebook = function (
  condition,
  callback
) {
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ facebookId: condition.id }, { email: condition.emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("============= CONDITION =============");
        console.log(condition);
        let values = {};
        values.facebookId = condition.googleId;
        values.email = condition.emails[0].value;
        value.nombre = condition.displayName || "SIN NOMBRE";
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("============= VALUES =============");
        console.log(values);

        setMaxListeners.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          callback(err, result);
        });
      }
    }
  );
};

module.exports = mongoose.model("Usuario", usuarioSchema);
